
package tp2;

import java.util.Observer;

/**
 *
 * @author martinabancens
 */
public class Main {

   
    public static void main(String[] args) {
        Observer screen = new Mecanico();

        Auto auto1 = new Auto("Daihatsu", "Cuore", 1993, 45, 22, 80);
        //Auto auto2 = new Auto("Mercedes", "C200", 2008, 70, 25, 60);
        auto1.addObserver(screen);
        //auto2.addObserver(screen);
        
        auto1.cambioAceite(100);
        auto1.cambioPresion(31);
        auto1.cambioAgua(100);
            
    }
    
}
