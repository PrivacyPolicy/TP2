/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp2;

import java.util.Observable;
import java.util.Observer;

public class Mecanico implements Observer {

    @Override
    public void update(Observable AutoNuevo, Object AutoViejo) {
        if(AutoViejo instanceof Auto){
            Auto Nuevito = (Auto)AutoNuevo;
            Auto Viejito = (Auto)AutoViejo;
            if(Nuevito.getNivelAceite()!=Viejito.getNivelAceite()){
                System.out.println("Cambio Aceite: De " + Viejito.getNivelAceite() + " a " + Nuevito.getNivelAceite());
            }
            if(Nuevito.getPresionNeumaticos()!=Viejito.getPresionNeumaticos()){
                System.out.println("Cambio Presion Neumaticos: De " + Viejito.getPresionNeumaticos() + " a " + Nuevito.getPresionNeumaticos());
            }
            if(Nuevito.getNivelAgua()!=Viejito.getNivelAgua()){
                System.out.println("Cambio Agua: De " + Viejito.getNivelAgua() + " a " + Nuevito.getNivelAgua());
            }
        }
        
    }

}