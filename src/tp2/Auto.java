/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp2;

import java.util.Observable;

/**
 *
 * @author martinabancens
 */
public class Auto extends Observable {
    private  String marca;
    private  String modelo;
    private  int anio;
    private  int nivelAceite;
    private  int presionNeumaticos;
    private  int nivelAgua;

    public Auto(String marca, String modelo, int anio, int nivelAceite, int presionNeumaticos, int nivelAgua) {
        this.marca = marca;
        this.modelo = modelo;
        this.anio = anio;
        this.nivelAceite = nivelAceite;
        this.presionNeumaticos = presionNeumaticos;
        this.nivelAgua = nivelAgua;
    }
    
    public void cambioAceite(int valor) {
        Auto ViejoAuto = new Auto(this.marca, this.modelo, this.anio, this.nivelAceite, this.presionNeumaticos, this.nivelAgua);
        this.nivelAceite = valor;
        
        setChanged();
        notifyObservers(ViejoAuto);
    }
    
    public void cambioPresion(int valor) {
        Auto ViejoAuto = new Auto(this.marca, this.modelo, this.anio, this.nivelAceite, this.presionNeumaticos, this.nivelAgua);
        this.presionNeumaticos = valor;
        
        setChanged();
        notifyObservers(ViejoAuto);
    }
    
    public void cambioAgua(int valor) {
        Auto ViejoAuto = new Auto(this.marca, this.modelo, this.anio, this.nivelAceite, this.presionNeumaticos, this.nivelAgua);
        this.nivelAgua = valor;
        
        setChanged();
        notifyObservers(ViejoAuto);
    }
    
    @Override
    public String toString() {
        return "Auto{" + "marca=" + marca + ", modelo=" + modelo + ", a\u00f1o=" + anio + ", nivelAceite=" + nivelAceite + ", presionNeumaticos=" + presionNeumaticos + ", nivelAgua=" + nivelAgua + '}';
    }

    
    
    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public int getAnio() {
        return anio;
    }

    public int getNivelAceite() {
        return nivelAceite;
    }

    public int getPresionNeumaticos() {
        return presionNeumaticos;
    }

    public int getNivelAgua() {
        return nivelAgua;
    }

    

    
}