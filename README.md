
En primer lugar se instancia un Observer y se le pasa por parametro a un objeto instanciado que extiende de Observable.
Luego dentro del metodo de la clase Observable se guarda una instancia del mismo Objeto y se actualiza el atributo correspondiente. Mas tarde se usa el metodo setChanged() para indicarle a la clase que implementa Observer que se realizo un cambio y "que lo mire", seguido por el metodo notifyObservers() que enviara como parametro a la instancia "vieja" guardada previamente, y el objeto actual.

Ya dentro de la clase Observer el metodo update se activara automaticamente recibiendo el objeto nuevo y viejo. Alli se vera si el objeto viejo pertenece a una instancia de la clase correspondiente y a traves de IF se determinara cual fue el atributo cambiado, mostrando el valor viejo y nuevo.
